import logo from "./logo.svg";
import "./App.css";
import axios from "axios";
import React, { useState, useEffect } from "react";
function App() {
  const [data, setData] = useState();

  const api = () => {
    axios
      .get("https://today.line.me/id/portaljson")
      .then((res) => {
        const result = res.data.result.categoryList
        setData(result);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    api();
  }, []);
  console.log(data);
  return <div className="App">
    {data && data.map((e,i) => (
      <small key={i} style={{margin: '1vw', background: 'gray'}}>{e.name}</small>
    ))}
  </div>;
}

export default App;
