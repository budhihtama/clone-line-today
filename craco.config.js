const path = require("path");

module.exports = {
	webpack: {
		alias: {
			"@": path.resolve(__dirname, "src/"),
			"@assets": path.resolve(__dirname, "src/assets"),
			"@modules": path.resolve(__dirname, "src/modules"),
		},
	},
};
